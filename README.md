# 边缘计算服务器分布策略设计代码和结果库
## 路径约定  
(1) 在代码文件中，首先可以将root_const.py 所在的位置放到环境变量中，这样我们可以随时调用该函数来获知路径的信息，从而将其用作相对变量获取的根据。  
(2) 在windows系统中操作相对繁琐一些，我们需要在环境变量或者系统变量中创建一个***PYTHONPATH***的变量，将root_const.py所在的路径添加，python在启动的时候会自动检测环境以及系统变量，将***PYTHONPATH***下的路径添加到python自己的环境变量中，但是注意手动在***PYTHONPATH***设置路径的时候，设置完成后有3个界面的确定，都要进行点击以后，该环境变量的添加才会生效。  

## 文件约定
(1) server_placement_main.py : 该文件为进行控制的主要文件，我们对于数据处理、策略仿真以及测试的几个部分都在这里面几个部分分别控制,可以一件产生所有文件。  
(2) data_extract.py : 该文件是将我们需要日期的数据从总的数据中分离出来使用。  
(3) basic_predeal.py : 该文件存有数据预处理类型的基类。  
(4) server_fre_predeal.py ： 该文件主要是对于基站负载数据进行预处理的子类(以basic_predeal.py中基类为核心)该文件对于流量的在每个基站的分布进行时间段的统计，我们可以控制统计的时间长度以及统计时间间隔。  
(5) server_placement_basic.py : 该文件主要是服务器部署策略的基类。  
(6) server_placement_ilp.py : 该文件为server_placement_basic.py中类的子类，主要通过线性规划的方式对于服务器的部署进行求解。  
(7) server_placement_ilpbalance.py : 该文件为server_placement_basic.py中类的子类, 通过将不同时刻的数据拆分开建立更多约束，获得一个更加具有鲁棒性的服务器的部署策略。  
(8) server_placement_random.py : 该文件为server_placement_basic.py中类的子类, 通过随机数直接随机的分配服务器的部署策略，是我们用于功能比较的策略。  
(9) server_placement_kmeans.py : 该文件为server_placement_basic.py中类的子类, 通过kmeans聚类的方式对于服务器部署，也是我们的比较方案。  
(10) server_placement_uniform.py : 该文件为server_placement_basic.py中类的子类,我们将服务器均匀地部署在基站中来得到服务器的部署方式。    
(11) server_beta_test.py:对于我们计算出服务器分布策略的Beta值进行计算。  
