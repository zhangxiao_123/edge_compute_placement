# -*- coding: utf-8 -*-
"""
Created on Sun Feb 14 17:15:56 2021

@author: 98330
"""
import numpy as np
import matplotlib.pyplot as plt

def load_data_float(file_path, file_name):
    f = open(file_path + file_name, "r")
    arr = []
    line = f.readline()
    while line:
        list_line = line.split("|")
        list_line = list(map(float,list_line))
        arr.append(list_line)
        line = f.readline()
    f.close()
#    print("文件读取成功")
    return arr
workload_all = load_data_float('./', '6_workload_all_base.txt')
workload_all_array = np.array(workload_all)
mean_list = []
std_list = []
for i in range(len(workload_all_array)):
    mean_list.append(np.mean(workload_all_array[i,:]))
    std_list.append(np.std(workload_all_array[i,:]))
plt.scatter(mean_list,std_list)
plt.show()
    