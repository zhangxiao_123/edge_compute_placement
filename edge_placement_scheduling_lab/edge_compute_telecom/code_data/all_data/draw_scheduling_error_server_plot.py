import os
import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams['font.sans-serif'] = ['Times New Roman']

styles = ['-', '-.', '--', ':', 'solid',
          'dashed', 'dotted', 'dashdot', 'dashed']
markers = [' ', '>', '8', '*', 'x', '+', 'p', 'D']
colors = ["red", "orange", "blue", "c", "cyan",
          "brown", "mediumvioletred", "dodgerblue", "green"]


def load_a_day_res(filename):
    """
    读取一个结果文件
    """
    with open(filename, 'r') as f:
        row_list = f.read().splitlines()
    a_day_res = row_list[0].split('|')
    a_day_res = [int(i) for i in a_day_res]
    return a_day_res



def make_res_table_statistics(filepath, hour, server_num_list):
    """
    将某方法结果的所有天跑出来的结果汇总成一个table
    然后得出统计量，提供画图数据
    """
    error_list = []
    for server_num in server_num_list:
        error_data = load_a_day_res(f'{filepath}/{hour}_{server_num}.txt')
        error_list.append(sum(error_data))
   
    res = (sorted(server_num_list), sorted(error_list, reverse = True))
    return res


def draw_plot(data_dict):
    """
    Input: 接受任意数量的数据，key充当画图的图例，value是画图用的原始数据
    """
    plt.figure(figsize=(8, 4))
    # 适配曲线数量
    count = 0
    for k, (server_num, error) in data_dict.items():
        print(k, server_num, error)
        # sns.lineplot(x, data, label=k, linestyle=styles[count], color=colors[count], linewidth=2.5)
        plt.plot(server_num, error, label=k, linestyle=styles[count], color=colors[count], linewidth=2.5)
        # plt.plot(server_num, error, label=k, linewidth=2.5)
        count += 1

    # plt.ylim(1000, 5000)
    # plt.xlim(7000, 12000)
    plt.yticks(fontsize=27)
    plt.xticks(fontsize=27)
    plt.xlabel("Number of servers", fontsize=27)
    plt.ylabel("Error", fontsize=27)
    plt.grid()
    plt.legend(fontsize=27)
    # plt.legend(ncol=5, loc='lower center', bbox_to_anchor=(0.6, 0), fontsize=15, framealpha=0.5,
    #     borderpad=0.1,handletextpad=0.1, handlelength=1, columnspacing=0.2)
    return plt


if __name__ == "__main__":

    time_0 =  make_res_table_statistics('server_scheduling_error_result', 0, [i for i in range(3000, 6001, 500)])
    time_1 =  make_res_table_statistics('server_scheduling_error_result', 1, [i for i in range(3000, 6501, 500)])
    time_2 =  make_res_table_statistics('server_scheduling_error_result', 2, [i for i in range(3000, 6501, 500)])
    time_3 =  make_res_table_statistics('server_scheduling_error_result', 3, [i for i in range(3000, 6501, 500)])
    time_4 =  make_res_table_statistics('server_scheduling_error_result', 4, [i for i in range(3000, 6501, 500)])
    time_5 =  make_res_table_statistics('server_scheduling_error_result', 5, [i for i in range(3000, 6501, 500)])
    time_6 =  make_res_table_statistics('server_scheduling_error_result', 6, [i for i in range(5000, 8001, 500)])
    time_7 =  make_res_table_statistics('server_scheduling_error_result', 7, [i for i in range(3000, 6501, 500)])
    time_8 =  make_res_table_statistics('server_scheduling_error_result', 8, [i for i in range(5000, 8001, 500)])
    time_9 =  make_res_table_statistics('server_scheduling_error_result', 9, [i for i in range(5000, 8001, 500)])
    time_10 =  make_res_table_statistics('server_scheduling_error_result', 10, [i for i in range(5000, 8001, 500)])
    time_11 =  make_res_table_statistics('server_scheduling_error_result', 11, [i for i in range(5000, 8001, 500)])
    time_12 =  make_res_table_statistics('server_scheduling_error_result', 12, [i for i in range(5000, 8001, 500)])
    time_13 =  make_res_table_statistics('server_scheduling_error_result', 13, [i for i in range(5000, 8001, 500)])
    time_14 =  make_res_table_statistics('server_scheduling_error_result', 14, [i for i in range(5000, 8001, 500)])
    time_15 =  make_res_table_statistics('server_scheduling_error_result', 15, [i for i in range(5000, 8001, 500)])
    time_16 =  make_res_table_statistics('server_scheduling_error_result', 16, [i for i in range(5000, 8001, 500)])
    time_17 =  make_res_table_statistics('server_scheduling_error_result', 17, [i for i in range(5000, 8001, 500)])
    time_18 =  make_res_table_statistics('server_scheduling_error_result', 18, [i for i in range(5000, 8001, 500)])
    time_19 =  make_res_table_statistics('server_scheduling_error_result', 19, [i for i in range(5000, 8001, 500)])
    time_20 =  make_res_table_statistics('server_scheduling_error_result', 20, [i for i in range(5000, 8001, 500)])
    time_21 =  make_res_table_statistics('server_scheduling_error_result', 21, [i for i in range(5000, 8001, 500)])
    time_22 =  make_res_table_statistics('server_scheduling_error_result', 22, [i for i in range(5000, 8001, 500)])
    time_23 =  make_res_table_statistics('server_scheduling_error_result', 23, [i for i in range(5000, 8001, 500)])
    
    data_dict = {
        'time_0': time_0,
        # 'time_1': time_1,
        # 'time_2': time_2,
        # 'time_3': time_3,
        # 'time_4': time_4,
        # 'time_5': time_5,
        # 'time_6': time_6,
        # 'time_7': time_7,
        'time_8': time_8,
        # 'time_9': time_9,
        # 'time_10': time_10,
        # 'time_11': time_11,
        'time_12': time_12,
        # 'time_13': time_13,
        # 'time_14': time_14,
        # 'time_15': time_15,
        # 'time_16': time_16,
        # 'time_17': time_17,
        # 'time_18': time_18,
        # 'time_19': time_19,
        # 'time_20': time_20,
        'time_21': time_21,
        # 'time_22': time_22,
        # 'time_23': time_23,
    }

    pt = draw_plot(data_dict)
    pt.tight_layout()
    file_name = __file__.split('/')[-1].split('.')[0]
    pt.savefig(f"{file_name}.pdf", bbox_inches='tight')
    pt.show()
