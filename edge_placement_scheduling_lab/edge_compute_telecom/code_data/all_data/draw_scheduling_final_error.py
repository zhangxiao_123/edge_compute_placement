import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

plt.rcParams['font.sans-serif'] = ['Times New Roman']

styles = ['-', '-.', '--', ':', 'solid',
          'dashed', 'dotted', 'dashdot', 'dashed']
markers = [' ', '>', '8', '*', 'x', '+', 'p', 'D']
colors = ["red", "orange", "blue", "c", "cyan",
          "brown", "mediumvioletred", "dodgerblue", "green"]


def load_a_day_res(filename):
    """
    读取一个结果文件
    """
    with open(filename, 'r') as f:
        row_list = f.read().splitlines()
    a_day_res = row_list[0].split('|')
    a_day_res = [int(float(i)) for i in a_day_res]
    return a_day_res


def by_seq(t):
    return int(t.split('_')[0])

def make_res_table_statistics(filepath):
    """
    将某方法结果的所有天跑出来的结果汇总成一个table
    然后得出统计量，提供画图数据
    """
    file_list = os.listdir(filepath)
    file_list = sorted(file_list, key = by_seq)

    res = []
    for filename in file_list:
        error_data = load_a_day_res(filepath + '/' + filename)
        res.append(error_data)
    return res


def draw_plot(data_dict):
    """
    Input: 接受任意数量的数据，key充当画图的图例，value是画图用的原始数据
    """
    plt.figure(figsize=(8, 4))
    # 适配曲线数量
    count = 0
    for k, error in data_dict.items():
        plt.plot(error, label=k, linestyle=styles[count], color=colors[count], linewidth=2.5)        
        # x = [i for i in range(len(error))]
        # plt.bar(x=x, height=error)
        count += 1

    # plt.ylim(1000, 5000)
    # plt.xlim(7000, 12000)
    plt.yticks(fontsize=27)
    plt.xticks(fontsize=27)
    plt.xlabel("time", fontsize=27)
    plt.ylabel("Error", fontsize=27)
    plt.grid()
    # plt.legend(fontsize=27)
    # plt.legend(ncol=5, loc='lower center', bbox_to_anchor=(0.6, 0), fontsize=15, framealpha=0.5,
    #     borderpad=0.1,handletextpad=0.1, handlelength=1, columnspacing=0.2)
    return plt


if __name__ == "__main__":

    final_error_table =  make_res_table_statistics('scheduling_final_error')
    final_error_list = []
    row_num = len(final_error_table)
    col_num = len(final_error_table[0])
    
    for j in range(col_num):
        for i in range(row_num):
            final_error_list.append(final_error_table[i][j])
    
    data_dict = {
        'final_error': final_error_list,
    }

    pt = draw_plot(data_dict)
    pt.tight_layout()
    file_name = __file__.split('/')[-1].split('.')[0]
    pt.savefig(f"{file_name}.pdf", bbox_inches='tight')
    pt.show()
