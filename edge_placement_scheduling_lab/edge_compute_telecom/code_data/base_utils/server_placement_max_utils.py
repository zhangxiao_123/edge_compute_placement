# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 19:19:19 2021

@author: 98330
"""
import math
import numpy as np
from base_utils.my_utils import load_data, save_list_2D
from base_utils.xlsx_txt_utils import get_colomn_infor0
import root_const
from root_const import ROOT_PATH

def load_distribution(lanti_1, long_1, lanti_2, long_2, load):
    """find the locations in the loca2 that are emerged in the loca1 
        
    param
    -----------
    lanti_1 : list
        store all the lantitude information of the loca1
    lanti_2 : list
        store all the lantitude information of the loca2
    long_1 : list
        store all the longtitude information of the loca1
    long_2 : list
        store all the longtitude information of the loca2
    load : list
        store all the load information of the loca2
            
    return
    ----------
    load_rebuild : list
        store the load in both loca1 and loca2, others are 0
        num : int
            the number of the loca included in both loca1 and loca2
       
    refer
    ----------
        Other func : None
    """
    base_num = len(lanti_1)
    num = 0
    load_rebuild = [0 for i in range(base_num)]
        
    for i in range(len(lanti_2)):    
        try:
            lanti_index = lanti_1.index(lanti_2[i])
        except:
            lanti_index = -1
        
        try:
            long_index = long_1.index(long_2[i])
        except:
            long_index = -2
                
        if lanti_index == long_index:
            load_rebuild[lanti_index] = load[i]
            num = num + 1
    return load_rebuild, num

def rebuild_frequ(path, file):
    tsv_information = load_data(path, file)
    lanti_list = get_colomn_infor0(tsv_information, 0)
    lanti_list = list(map(float, lanti_list))
    long_list = get_colomn_infor0(tsv_information, 1)
    long_list = list(map(float, long_list))
    frequ_list = get_colomn_infor0(tsv_information, 2)
    frequ_list = list(map(float, frequ_list))
    tsv_information = load_data(ROOT_PATH + '/all_data/telecom_all_data_frequ/', 'data_6 00_frequ.txt')
    lanti_list1 = get_colomn_infor0(tsv_information, 0)
    lanti_list1 = list(map(float, lanti_list1))
    long_list1 = get_colomn_infor0(tsv_information, 1)
    long_list1 = list(map(float, long_list1))
    load_rebuild, num = load_distribution(lanti_list1, long_list1, lanti_list, long_list, frequ_list)
    return lanti_list1, long_list1, load_rebuild

    
def max_distribution(lanti1, long1, time_interval):
    """
    rebuild the data to change one day data into several part
    """
    time_list = []
    number_name = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    for index in range(time_interval):
        data_ten_num = math.floor(index / 10)
        data_num = index % 10
        if data_ten_num == 0:
            time_list.append(number_name[data_num])
        else:
            time_list.append(number_name[data_ten_num] + number_name[data_num])
    path = ROOT_PATH + '/data/telecom_data/6_1TO5 fre/'
    final_rebuild = []
        
    for i in range(len(time_list)):
        name =  '6_01TO05 ' + time_list[i] + '_frequ.txt'
        tsv_information = load_data(path, name)
        lanti_list = get_colomn_infor0(tsv_information, 0)
        lanti2 = list(map(float, lanti_list))
        long_list = get_colomn_infor0(tsv_information, 1)
        long2 = list(map(float, long_list))
        frequ_list = get_colomn_infor0(tsv_information, 2)
        frequ = list(map(float, frequ_list))
        load_rebuild, num = load_distribution(lanti1, long1, lanti2, long2, frequ)
        final_rebuild.append(list(np.array(load_rebuild)))
    
    final_rebuild_array = np.array(final_rebuild)
    max_rebuild = []
    for i in range(len(lanti1)):
        temp = []
        temp.append(lanti1[i])
        temp.append(long1[i])
        temp.append(np.max(final_rebuild_array[:,i]))
        max_rebuild.append(temp)
    return max_rebuild    


#tsv_information = load_data(ROOT_PATH + '/data/telecom_data/6_6 fre/', '6_01TO05_sum 0_frequ.txt')
#lanti_list = get_colomn_infor0(tsv_information, 0)
#lanti_list = list(map(float, lanti_list))
#long_list = get_colomn_infor0(tsv_information, 1)
#long_list = list(map(float, long_list))
#frequ_list = get_colomn_infor0(tsv_information, 2)
#frequ_list = list(map(float, frequ_list))
#
#max_rebuild = max_distribution(lanti_list, long_list, 24)
#save_list_2D(ROOT_PATH + '/data/telecom_data/6_6 fre/', '6_01TO05_max 0_frequ.txt', max_rebuild)
