# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 13:01:42 2021

@author: 98330
"""

#import xlrd
import os
import root_const
#from datetime import datetime
#from xlrd import xldate_as_tuple
from root_const import ROOT_PATH
from data_predeal.server_fre_predeal import PreDealServerFre
from base_utils.my_utils import save_data_switch, load_data, add_data_switch, save_list_2D

#def xlsx_to_txt(xlsx_path, xlsx_file, txt_path, txt_file):
#    workbook = xlrd.open_workbook(xlsx_path + xlsx_file)
#    print('工作表数量:', workbook.nsheets) 
#    table = workbook.sheets()[0]
#    f = save_data_switch(txt_path, txt_file, table.row_values(0))
#    for worksheet in workbook.sheets(): 
#        print("Worksheet name:", worksheet.name, "\tRows:",worksheet.nrows, "\tColumns:", worksheet.ncols)
#        for index in range(1, worksheet.nrows):
#            if worksheet.cell_value(index, 4) != '':
#                temp_value = worksheet.row_values(index)
#                temp_value[2] = datetime(*xldate_as_tuple(worksheet.cell_value(index, 2), 0))
#                temp_value[3] = datetime(*xldate_as_tuple(worksheet.cell_value(index, 3), 0))
#                temp_value[2] = temp_value[2].strftime("%Y/%m/%d_%H:%M:%S")
#                temp_value[3] = temp_value[3].strftime("%Y/%m/%d_%H:%M:%S")
#                add_data_switch(f, temp_value, 0)
#    f.close()
    
def data_extract_txt(origin_path, origin_file, extract_path, extract_file, date_down, date_up):
    total_data = load_data(origin_path, origin_file)
    f = save_data_switch(extract_path, extract_file, total_data[0])
    for i in range(1,len(total_data)):
        data_temp = total_data[i]
        if (int(float(data_temp[1])) >= date_down and int(float(data_temp[1])) <= date_up):
            add_data_switch(f, data_temp, 0)
    f.close()

def data_extract_division(origin_path, extract_path, division_time):
    file_name_list = os.listdir(origin_path)
    filename = file_name_list[file_name_list.index('data_6.1~6.30_.txt')]
    for i in range(30 - division_time + 1):
        name1 = str(i + 1)
        name2 = str(i + division_time)
        if i + 1 < 10:
            name1 = '0' + str(i + 1)
        if i + division_time < 10:
            name2 = '0' + str(i + division_time)
        data_extract_txt(origin_path, filename,
                         extract_path, 'data_6_' + name1 + 'to' + name2 + '.txt', i + 1,i + division_time)

def data_frequency_calculate(origin_path, output_path, start_time, time_num):
    file_name_list = os.listdir(origin_path)
    for i in file_name_list:
        name1 = i[0:str.find(i,'.')]
        frequency_pre_deal = PreDealServerFre()
        txt_information = load_data(origin_path, i)
        s_time_list = get_colomn_infor1(txt_information, 2)
        e_time_list = get_colomn_infor1(txt_information, 3)
        location_list = get_colomn_infor1(txt_information, 4)
        frequency_total = frequency_pre_deal.frequency_result(s_time_list, e_time_list, location_list, start_time, time_num)
        for i in range(len(frequency_total)):
            name2 = str(i) + '_frequ.txt' 
            if i < 10:
                name2 = '0' + str(i) + '_frequ.txt' 
            save_list_2D(output_path, name1 + ' ' + name2, frequency_total[i])

def get_colomn_infor1(data, index):
    colomn_infor = []
    for i in range(1,len(data)):
        colomn_infor.append(data[i][index])
    return colomn_infor

def get_colomn_infor0(data, index):
    colomn_infor = []
    for i in range(0,len(data)):
        colomn_infor.append(data[i][index])
    return colomn_infor        

def load_accurate_date(path, date):
    path_file_list = os.listdir(path)
    lanti_sum = []
    long_sum = []
    frequ_sum = []
    for i in path_file_list:
        if i[7:9] == date:
            tsv_information = load_data(path, i)
            lanti_list = get_colomn_infor0(tsv_information, 0)
            lanti_list = list(map(float, lanti_list))
            long_list = get_colomn_infor0(tsv_information, 1)
            long_list = list(map(float, long_list))
            frequ_list = get_colomn_infor0(tsv_information, 2)
            frequ_list = list(map(float, frequ_list))
            lanti_sum.append(lanti_list)
            long_sum.append(long_list)
            frequ_sum.append(frequ_list)
    return lanti_sum, long_sum, frequ_sum

def load_follow_date(path, date, time_num):
    path_file_list = os.listdir(path)
    lanti_sum = []
    long_sum = []
    frequ_sum = []
    count = 0
    for i in range(len(path_file_list)):
        if path_file_list[i][7:9] == date:
            if count == time_num:
                break
            if count == 0:
                tsv_information = load_data(path, path_file_list[i - 1])
                lanti_list = get_colomn_infor0(tsv_information, 0)
                lanti_list = list(map(float, lanti_list))
                long_list = get_colomn_infor0(tsv_information, 1)
                long_list = list(map(float, long_list))
                frequ_list = get_colomn_infor0(tsv_information, 2)
                frequ_list = list(map(float, frequ_list))
                lanti_sum.append(lanti_list)
                long_sum.append(long_list)
                frequ_sum.append(frequ_list)
                count = count + 1
            tsv_information = load_data(path, path_file_list[i])
            lanti_list = get_colomn_infor0(tsv_information, 0)
            lanti_list = list(map(float, lanti_list))
            long_list = get_colomn_infor0(tsv_information, 1)
            long_list = list(map(float, long_list))
            frequ_list = get_colomn_infor0(tsv_information, 2)
            frequ_list = list(map(float, frequ_list))
            lanti_sum.append(lanti_list)
            long_sum.append(long_list)
            frequ_sum.append(frequ_list)
            count = count + 1
    return lanti_sum, long_sum, frequ_sum


if __name__ == '__main__':
    a = list(range(17, 30))
    print(a)
#    ORIGIN_PATH = 'D:/spider_file/edge_placement_scheduling_lab/edge_compute_telecom/code_data/all_data/telecom_data_xlsx/'
#    OUTPUT_PATH = 'D:/spider_file/edge_placement_scheduling_lab/edge_compute_telecom/code_data/all_data/telecom_data_txt/'
#    EXTRACT_PATH = 'D:/spider_file/edge_placement_scheduling_lab/edge_compute_telecom/code_data/all_data/telecom_data_division_one_day/'
##    xlsx_to_txt(ORIGIN_PATH, 'data_6.1~6.30_.xlsx', OUTPUT_PATH, 'data_6.1~6.30_.txt')
#    for i in range(30):
#        if i + 1 < 10:
#            name = '0' + str(i + 1)
#        else:
#            name = str(i + 1)
#        data_extract_txt(OUTPUT_PATH, 'data_6.1~6.30_.txt', EXTRACT_PATH, 'data_6_' + name + '.txt', i + 1,i + 1)
#    data =  load_data(ROOT_PATH + '/data/telecom_data/', 'data_6.1~6.30_.txt')