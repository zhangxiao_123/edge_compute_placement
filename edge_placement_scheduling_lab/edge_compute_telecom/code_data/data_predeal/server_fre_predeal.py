# -*- coding: utf-8 -*-


import math
from data_predeal.basic_predeal import PreDealData

class PreDealServerFre(PreDealData):
    
    """
    Sub class to deal with the data and transform the data into the format composed with the location and the frequency
    """
    
    def __init__(self):
        PreDealData.__init__(self)
    
    def statistic_user_inhour(self, time_start_list, time_end_list, start_time, time_part_num):
        """Statistic the load of the each flow station in different time of one day
            
        param : 
        ----------
            time_start_list :　list
                The start time list of all the flows of the day
            time_end_list :　list
                The end time list of all the flows of the day
            start_time : string
                The beginning of the first time part
            time_part_num : int
                The number of the time parts
        
        return :
        ----------
            time_statistic : list
                 structure : [[[],[]],[[],[]],.....,[[],[]]]
                 len : time_part_num
                 time_statistic[index][0] : The signal of each flow to show which time part it is included
                 time_statistic[index][1] : The user's time consumption of each flow
        
        refer:
        -----------
            Other func : None
        """
        time_statistic = []
        start_time_hour = int(start_time[0 : start_time.find(':')])
        start_time_minute = int(start_time[start_time.find(':')+1 : start_time.find(':')+3])
        start_time_num = start_time_hour + start_time_minute / 60
        time_part = 24 / time_part_num
        
        for index in range(time_part_num):       
            time_statistic.append([])
            time_statistic[index].append([])
            time_statistic[index].append([])  
        # get the time_signal and load for each time flow
        for num_index in range(len(time_start_list)):
            start = time_start_list[num_index][time_start_list[num_index].find('_'):]
            start_hour = int(start[1:start.find(':')])
            start_minute = int(start[start.find(':') + 1 : start.find(':') + 3])
            start_num = start_hour + start_minute / 60
            if start_num >= 0 and start_num < start_time_num:
                start_num = start_num + 24
            
            end = time_end_list[num_index][time_end_list[num_index].find('_'):]
            end_hour = int(end[1 : end.find(':')])
            end_minute = int(end[end.find(':') + 1 : end.find(':') + 3])
            end_num = end_hour + end_minute / 60    
            if end_num >= 0 and end_num < start_time_num:
                end_num = end_num + 24
            
            start_index = math.floor((start_num - start_time_num) / time_part)
            end_index = math.floor((end_num - start_time_num) / time_part)
            # The end time and start time may be not included in the same part
            while(end_index != start_index):
                if(end_index > start_index):
                    time_statistic[end_index][0].append(num_index)
                    time_statistic[end_index][1].append((end_num - (start_time_num + end_index * time_part)))
                    end_num = start_time_num + end_index * time_part
                    end_index = end_index - 1
                else:
                    time_statistic[start_index][0].append(num_index)
                    time_statistic[start_index][1].append(((start_time_num + (start_index + 1) * time_part) - start_num))
                    start_num = (start_time_num + (start_index + 1) * time_part) % 24
                    start_index = (start_index + 1) % time_part_num
        
            time_statistic[start_index][0].append(num_index)
            if(end_num < start_num):
                end_num = end_num + 24
            time_statistic[start_index][1].append((end_num - start_num))
        return time_statistic

    def statistic_frequency(self, list_index, location_list):
        """Statitic the load of the same base station
        
        param
        ----------
            list_index : list
                The statistic result about the load of the flow in one time part
                structure :　[[],[]]
                list_index[0] : The signal of each flow to show which time part it is included
                list_index[1] : The user's time consumption of each flow
            location_list : list
                All the information of the locations about the base station
                structure : [content1, content2, content3,......, contentn]
                content : string ---> (lantitude/longtitude)
        
        return
        ----------
            statis_condition : list
                structure : [[ , , ],[ , , ]......[ , , ]]
                statis_condition[index] ---> [lantitude, longitude, load]
        
        refer
        ----------
            Other func : None
        """
        list_num = list_index
        list_final_statis = []
        list_final_statis.append([])
        list_final_statis.append([])
        list_new = list()
        list_new_num = list()
        list_index = range(len(list_num[1]))
        statis_condition = []
        statis_new = []
        # Get the load for each base station
        while list_num[0] != list():
            list_new.clear()
            list_new_num.clear()
            list_len = len(list_num[0])
            list_standard = location_list[list_num[0][0]]
            # 用户位置信息存在空白，需要排除空白信息
            if isinstance(list_standard, float):
                list_num[0].remove(list_num[0][0])
                continue
            load = 0
            
            for seq in range(list_len):
                if list_standard == location_list[list_num[0][seq]]:
                    list_new.append(list_num[0][seq])
                    list_new_num.append(list_index[seq])
                    load = load + list_num[1][list_index[seq]]
            list_num[0] = list(set(list_num[0]) - set(list_new))
            list_index = list(set(list_index) - set(list_new_num))
            list_num[0].sort()
            list_final_statis[0].append(list(list_new))
            list_final_statis[1].append(load)
            
        for statis_index in range(len(list_final_statis[0])):
            statis_new = []
            loca_index = list_final_statis[0][statis_index][0]
            statis_new.append(float(location_list[loca_index][0:location_list[loca_index].find('/')]))
            statis_new.append(float(location_list[loca_index][location_list[loca_index].find('/') + 1 : ]))
            statis_new.append(list_final_statis[1][statis_index])
            statis_condition.append(statis_new)
            
        return statis_condition 
    
    def frequency_result(self, time_start_list, time_end_list, location_list, start_time, time_part_num):
        """statistic all the load information in all time parts
        
        param
        ----------
            time_start_list : list
                All the start time included in the list
                structure : [content1, content2, content3,......, contentn]
                content : string ---> 00:00
            time_end_list : list
                All the end time included in the list
                structure : [content1, content2, content3,......, contentn]
                content : string ---> 00:00
            location_list : list
                All the information of the locations about the base station
                structure : [content1, content2, content3,......, contentn]
                content : string ---> (lantitude/longtitude)
            start_time : string ---> 00:00
                Choose a start time as the beginning of the first part
            time_part_num : int
                The number to divide a day
        
        return
        ----------
            frequency_total : list
            structure : [[statis_condition1],[statis_condition2],.....,[statis_condition time_part_num]]
            statis_condition : list
                structure : [[ , , ],[ , , ]......[ , , ]]
                statis_condition[index] ---> [lantitude, longitude, load]
        
        refer
        ----------
            Other func : self.statistic_user_inhour, self.statistic_frequency
        
        """
        
        time_statistic = self.statistic_user_inhour(time_start_list, time_end_list, start_time, time_part_num)
        frequency_total = []
        
        for index in range(time_part_num):
            frequency_num = self.statistic_frequency(time_statistic[index], location_list)
            frequency_total.append(frequency_num)
            
        return frequency_total 



