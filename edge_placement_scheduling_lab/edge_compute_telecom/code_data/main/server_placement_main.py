# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 15:53:07 2021

@author: 98330
"""
import numpy as np
import root_const
import os
from root_const import ROOT_PATH
import matplotlib.pyplot as plt
from base_utils.my_utils import load_data, save_list_2D, save_data, list_to_2D, array_to_list, load_data_float, load_json
from base_utils.xlsx_txt_utils import data_extract_txt, get_colomn_infor1, get_colomn_infor0, data_extract_division, data_frequency_calculate,load_accurate_date, load_follow_date
from data_predeal.server_fre_predeal import PreDealServerFre
from base_utils.server_placement_max_utils import rebuild_frequ
from min_cost_flow.min_cost_flow import GetAccurateFlow
from server_placement.server_placement_ilp import ServerPlacementILP
from server_placement.server_placement_random import ServerPlacementRandom  
from server_placement.server_placement_kmeans import ServerPlacementKmeans
from server_placement.server_placement_uniform import ServerPlacementUniform
from server_placement.server_placement_ilpbalance import ServerPlacementILPBalance
from server_scheduling.server_switch_demand_predict import ServerSwitchDemandPredict
from server_scheduling.server_switch_energy import ServerSwitchEnergy
from server_scheduling.server_switch_demand_follow import ServerSwitchDemandFollow
from server_scheduling.server_switch_day_night import ServerSwitchDayNight
from server_scheduling_evaluation.server_scheduling_evaluation_energy import ServerSchedulingEvalutionEnergy



print("Part1: creat server placement result of several methods")
#lanti_list, long_list, load_rebuild = rebuild_frequ(ROOT_PATH + '/all_data/telecom_fifteen_data_frequ/', 'data_6_01to15 00_frequ.txt')
#loca_array = np.zeros((len(lanti_list),2))
#loca_array[:,0] = np.array(lanti_list)
#loca_array[:,1] = np.array(long_list)
#adjust_rate = 3 * 24
#load_rebuild_max_flow = list(np.array(load_rebuild) / adjust_rate)
#Integer programming------distance first
#server_placement_ilp = ServerPlacementILP(loca_array, 50, 50, 8000, load_rebuild, adjust_rate)
#a = server_placement_ilp.find_near_loca(5)
#server_placement_ilp.find_same_receiver_union()
#u_list, y_list_1, final_optimal = server_placement_ilp.server_placement()
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_1, 
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_ilp_result.txt')
#max flow result-------distance first
#u_list_2, y_list_2, final_optimal_2 = server_placement_ilp.server_placement_float()
#u_list_2, y_list_2 = server_placement_ilp.server_placement_float_best2(final_optimal_2)
#total_server, y_list_2 = GetAccurateFlow(u_list_2, y_list_2, load_rebuild_max_flow, final_optimal_2)
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_2,
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_max_flow_best_result.txt')
#max flow result-------key nodes first
#u_list_3, y_list_3, final_optimal_3 = server_placement_ilp.server_placement_float_relative()
#total_server, y_list_3 = GetAccurateFlow(u_list_3, y_list_3, load_rebuild_max_flow, final_optimal_3)
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_3,
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_max_flow_relative_result.txt')
#Integer programming------key_nodes first
#u_list, y_list_4, final_optimal_4 = server_placement_ilp.server_placement()
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_4, 
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_ilp_relative_result.txt')
#random distrabution
#server_placement_random = ServerPlacementRandom(len(lanti_list), 8000, 50, 0)
#y_list_5 = server_placement_random.server_placement()
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_5, 
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_random_result.txt')
#k_means distribution
#server_placement_kmeans = ServerPlacementKmeans(loca_array, 1000, load_rebuild, 8000)
#list_final, y_list_6 = server_placement_kmeans.server_placement()
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_6, 
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_k_means_result.txt')
#uniform distribution
#server_placement_uniform = ServerPlacementUniform(lanti_list, long_list, 8000)
#y_list_7 = server_placement_uniform.server_placement()
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_7, 
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_uniform_result.txt')

print("Evaluate server placement by same server scheduling idea")
txt_information = load_data(ROOT_PATH + '/all_data/server_placement_result/', 'server_max_flow_best_result.txt')
lanti1 = get_colomn_infor0(txt_information, 0)
base_num = len(lanti1)
lanti1 = list(map(float,lanti1))
long1 = get_colomn_infor0(txt_information, 1)
long1 = list(map(float,long1))
loca_array = np.zeros((len(lanti1),2))
loca_array[:,0] = np.array(lanti1)
loca_array[:,1] = np.array(long1)
server_placement_max_flow_best = get_colomn_infor0(txt_information, 3)
server_placement_max_flow_best = list(map(float,server_placement_max_flow_best))
server_scheduling_array = np.zeros((24,len(server_placement_max_flow_best)))
for i in range(24):
    server_scheduling_array[i,:] = np.array(server_placement_max_flow_best) 
for i in range(16,31):
    lanti_list, long_list, frequ_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
    lanti_history_list, long_history_list, frequ_history_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_five_day_frequ/', str(i - 5))
    serverswitch = ServerSwitchEnergy(loca_array, 50, 50, lanti1, long1, server_placement_max_flow_best,
                                    24, lanti_history_list, long_history_list, frequ_history_list,
                                    1, lanti_list, long_list, frequ_list, 5)
    predict_scheduling_array = server_scheduling_array
    failed_result_max_flow_best, u_result, service_num = serverswitch.failed_statistic(predict_scheduling_array, 1)
    save_data(ROOT_PATH  + '/all_data/server_placement_result/server_placement_test_result/max_flow_best_error/', 
                                    'energy_' + str(i) + '_use100_error.txt', failed_result_max_flow_best)
    
txt_information = load_data(ROOT_PATH + '/all_data/server_placement_result/', 'server_max_flow_result.txt')
server_placement_max_flow = get_colomn_infor0(txt_information, 3)
server_placement_max_flow = list(map(float,server_placement_max_flow))
server_scheduling_array = np.zeros((24,len(server_placement_max_flow)))
for i in range(24):
    server_scheduling_array[i,:] = np.array(server_placement_max_flow) 
for i in range(16,31):
    lanti_list, long_list, frequ_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
    lanti_history_list, long_history_list, frequ_history_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_five_day_frequ/', str(i - 5))

    serverswitch = ServerSwitchEnergy(loca_array, 50, 50, lanti1, long1, server_placement_max_flow,
                                    24, lanti_history_list, long_history_list, frequ_history_list,
                                    1, lanti_list, long_list, frequ_list, 5)        
    predict_scheduling_array = server_scheduling_array
#    list_failed_max = serverswitch.server_overload(predict_scheduling_array,1)
    failed_result_max_flow, u_result,service_num = serverswitch.failed_statistic(predict_scheduling_array, 1)
    save_data(ROOT_PATH  + '/all_data/server_placement_result/server_placement_test_result/max_flow_error/', 
                                    'energy_' + str(i) + '_use100_error.txt', failed_result_max_flow)

txt_information = load_data(ROOT_PATH + '/all_data/server_placement_result/', 'server_ilp_result.txt')
server_placement_ilp = get_colomn_infor0(txt_information, 3)
server_placement_ilp = list(map(float,server_placement_ilp))
server_scheduling_array = np.zeros((24,len(server_placement_ilp)))
for i in range(24):
    server_scheduling_array[i,:] = np.array(server_placement_ilp) 
for i in range(16,31):
    lanti_list, long_list, frequ_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
    lanti_history_list, long_history_list, frequ_history_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_five_day_frequ/', str(i - 5))

    serverswitch = ServerSwitchEnergy(loca_array, 50, 50, lanti1, long1, server_placement_ilp,
                                    24, lanti_history_list, long_history_list, frequ_history_list,
                                    1, lanti_list, long_list, frequ_list, 5)        
    predict_scheduling_array = server_scheduling_array
#    list_failed_max = serverswitch.server_overload(predict_scheduling_array,1)
    failed_result_ilp, u_result,service_num = serverswitch.failed_statistic(predict_scheduling_array, 1)
    save_data(ROOT_PATH  + '/all_data/server_placement_result/server_placement_test_result/ilp_error/', 
                                    'energy_' + str(i) + '_use100_error.txt', failed_result_ilp)
    
txt_information = load_data(ROOT_PATH + '/all_data/server_placement_result/', 'server_random_result.txt')
server_placement_random = get_colomn_infor0(txt_information, 3)
server_placement_random = list(map(float,server_placement_random))
server_scheduling_array = np.zeros((24,len(server_placement_random)))
for i in range(24):
    server_scheduling_array[i,:] = np.array(server_placement_random) 
for i in range(16,31):
    lanti_list, long_list, frequ_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
    lanti_history_list, long_history_list, frequ_history_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_five_day_frequ/', str(i - 5))
    serverswitch = ServerSwitchEnergy(loca_array, 50, 50, lanti1, long1, server_placement_random,
                                    24, lanti_history_list, long_history_list, frequ_history_list,
                                    1, lanti_list, long_list, frequ_list, 5)        
    predict_scheduling_array = server_scheduling_array
#    list_failed_max = serverswitch.server_overload(predict_scheduling_array,1)
    failed_result_random, u_result,service_num = serverswitch.failed_statistic(predict_scheduling_array, 1)
    save_data(ROOT_PATH  + '/all_data/server_placement_result/server_placement_test_result/random_error/', 
                                    'energy_' + str(i) + '_use100_error.txt', failed_result_random)

txt_information = load_data(ROOT_PATH + '/all_data/server_placement_result/', 'server_k_means_result.txt')
server_placement_k_means = get_colomn_infor0(txt_information, 3)
server_placement_k_means = list(map(float,server_placement_k_means))
server_scheduling_array = np.zeros((24,len(server_placement_k_means)))
for i in range(24):
    server_scheduling_array[i,:] = np.array(server_placement_k_means) 
for i in range(16,31):
    lanti_list, long_list, frequ_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
    lanti_history_list, long_history_list, frequ_history_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_five_day_frequ/', str(i - 5))
    serverswitch = ServerSwitchEnergy(loca_array, 50, 50, lanti1, long1, server_placement_k_means,
                                    24, lanti_history_list, long_history_list, frequ_history_list,
                                    1, lanti_list, long_list, frequ_list, 5)        
    predict_scheduling_array = server_scheduling_array
#    list_failed_max = serverswitch.server_overload(predict_scheduling_array,1)
    failed_result_k_means, u_result, service_num = serverswitch.failed_statistic(predict_scheduling_array, 1)
    save_data(ROOT_PATH  + '/all_data/server_placement_result/server_placement_test_result/k_means_error/', 
                                    'energy_' + str(i) + '_use100_error.txt', failed_result_k_means)

txt_information = load_data(ROOT_PATH + '/all_data/server_placement_result/', 'server_uniform_result.txt')
server_placement_uniform = get_colomn_infor0(txt_information, 3)
server_placement_uniform = list(map(float,server_placement_uniform))
server_scheduling_array = np.zeros((24,len(server_placement_uniform)))
for i in range(24):
    server_scheduling_array[i,:] = np.array(server_placement_uniform) 
for i in range(16,31):
    lanti_list, long_list, frequ_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
    lanti_history_list, long_history_list, frequ_history_list = load_accurate_date(
                                    ROOT_PATH + '/all_data/data_division_five_day_frequ/', str(i - 5))
    serverswitch = ServerSwitchEnergy(loca_array, 50, 50, lanti1, long1, server_placement_uniform,
                                    24, lanti_history_list, long_history_list, frequ_history_list,
                                    1, lanti_list, long_list, frequ_list, 5)        
    predict_scheduling_array = server_scheduling_array
#    list_failed_max = serverswitch.server_overload(predict_scheduling_array,1)
    failed_result_uniform, u_result, service_num = serverswitch.failed_statistic(predict_scheduling_array, 1)
    save_data(ROOT_PATH  + '/all_data/server_placement_result/server_placement_test_result/uniform_error/', 
                                    'energy_' + str(i) + '_use100_error.txt', failed_result_uniform)
    

