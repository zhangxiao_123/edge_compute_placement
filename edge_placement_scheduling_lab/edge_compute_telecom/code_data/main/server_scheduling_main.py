# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 18:07:11 2021

@author: 98330
"""

import numpy as np
import root_const
import os
from root_const import ROOT_PATH
import matplotlib.pyplot as plt
from base_utils.my_utils import load_data, save_list_2D, save_data, list_to_2D, array_to_list, load_data_float, load_json
from base_utils.xlsx_txt_utils import data_extract_txt, get_colomn_infor1, get_colomn_infor0, data_extract_division, data_frequency_calculate,load_accurate_date, load_follow_date
from data_predeal.server_fre_predeal import PreDealServerFre
from base_utils.server_placement_max_utils import rebuild_frequ
from min_cost_flow.min_cost_flow import GetAccurateFlow
from server_placement.server_placement_ilp import ServerPlacementILP
from server_placement.server_placement_random import ServerPlacementRandom  
from server_placement.server_placement_kmeans import ServerPlacementKmeans
from server_placement.server_placement_uniform import ServerPlacementUniform
from server_placement.server_placement_ilpbalance import ServerPlacementILPBalance
from server_scheduling.server_switch_demand_predict import ServerSwitchDemandPredict
from server_scheduling.server_switch_energy import ServerSwitchEnergy
from server_scheduling.server_switch_demand_follow import ServerSwitchDemandFollow
from server_scheduling.server_switch_day_night import ServerSwitchDayNight
from server_scheduling_evaluation.server_scheduling_evaluation_energy import ServerSchedulingEvalutionEnergy


print('part1: Aiming to extract the data we want to get')
#total_data = load_data(ROOT_PATH + '/all_data/telecom_data_txt/', 'data_6.1~6.30_.txt')
#extract the data of single day
#data_extract_division(ROOT_PATH + '/all_data/telecom_data_txt/', ROOT_PATH + '/all_data/telecom_data_division_one_day/', 1)
#extract the data of five days
#data_extract_division(ROOT_PATH + '/all_data/telecom_data_txt/', ROOT_PATH + '/all_data/telecom_data_division_five_day/', 5)
#extract the data of fifteen days
#data_extract_division(ROOT_PATH + '/all_data/telecom_data_txt/', ROOT_PATH + '/all_data/telecom_data_division_fifteen_day/', 15)

print('part2: Aiming to get the frequency information we want')
#data_frequency_calculate(ROOT_PATH + '/all_data/telecom_data_division_one_day/', 
#                         ROOT_PATH + '/all_data/data_division_one_day_frequ/', '0:00', 24)
#data_frequency_calculate(ROOT_PATH + '/all_data/telecom_data_division_five_day/', 
#                         ROOT_PATH + '/all_data/data_division_five_day_frequ/', '0:00', 24)
#data_frequency_calculate(ROOT_PATH + '/all_data/telecom_data_txt/', 
#                         ROOT_PATH + '/all_data/telecom_all_data_frequ/', '0:00', 1)
#data_frequency_calculate(ROOT_PATH + '/all_data/telecom_data_fifteen_txt/', 
#                         ROOT_PATH + '/all_data/telecom_fifteen_data_frequ/', '0:00', 1)

print('part3: Aiming to get the server placement method')
#lanti_list, long_list, load_rebuild = rebuild_frequ(ROOT_PATH + '/all_data/telecom_fifteen_data_frequ/', 'data_6_01to15 00_frequ.txt')
#loca_array = np.zeros((len(lanti_list),2))
#loca_array[:,0] = np.array(lanti_list)
#loca_array[:,1] = np.array(long_list)
#adjust_rate = 3 * 24
#load_rebuild_max_flow = list(np.array(load_rebuild) / adjust_rate)
#
#server_placement_ilp = ServerPlacementILP(loca_array, 80, 50, 8000, load_rebuild, adjust_rate)
#server_placement_ilp.find_same_receiver_union()

#u_list, y_list_1, final_optimal = server_placement_ilp.server_placement()
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_1, 
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_ilp_result.txt')
#u_list_2, y_list_2, final_optimal_2 = server_placement_ilp.server_placement_float()
#total_server, y_list_2 = GetAccurateFlow(u_list_2, y_list_2, load_rebuild_max_flow, final_optimal_2)
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_2,
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_max_flow_result.txt')
#u_list_3, y_list_3, final_optimal_3 = server_placement_ilp.server_placement_float_relative()
#total_server, y_list_3 = GetAccurateFlow(u_list_3, y_list_3, load_rebuild_max_flow, final_optimal_3)
#list_to_2D(lanti_list, long_list, load_rebuild, y_list_3,
#           ROOT_PATH + '/all_data/server_placement_result/', 'server_max_flow_relative_result.txt')


print('part4: Aiming to evaluate the server scheduling method')
txt_information = load_data(ROOT_PATH + '/all_data/server_placement_result/', 'server_max_flow_relative_result.txt')
lanti1 = get_colomn_infor0(txt_information, 0)
base_num = len(lanti1)
#lanti1 = list(map(float,lanti1))
#long1 = get_colomn_infor0(txt_information, 1)
#long1 = list(map(float,long1))
#loca_array = np.zeros((len(lanti1),2))
#loca_array[:,0] = np.array(lanti1)
#loca_array[:,1] = np.array(long1)
#server_placement_max_flow = get_colomn_infor0(txt_information, 3)
#server_placement_max_flow = list(map(float,server_placement_max_flow))
#
#for i in range(16,31):
#    lanti_list, long_list, frequ_list = load_accurate_date(
#                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
#    serverswitch = ServerSwitchDayNight(loca_array, 80, 50, lanti1, long1, server_placement_max_flow,
#                                    24, lanti_list, long_list, frequ_list, 5)
#    result_list = serverswitch.server_switch('8:00', '21:00', 0.6)
#    result = array_to_list(result_list)
#    save_list_2D(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_day_night_relative/',
#                                    'server_scheduling_' + str(i) + '_day_night60.txt', result)
#    predict_scheduling = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_day_night_relative/', 
#                                    'server_scheduling_' + str(i) + '_day_night60.txt')
#    predict_scheduling_array = np.array(predict_scheduling)
#    sum_error0, error0 = serverswitch.service_failure_statistic(predict_scheduling_array, 1)
#    save_data(ROOT_PATH + '/all_data/server_scheduling_result/day_night_relative_error/', 'day_night_' + str(i) + '_60_error.txt', list(sum_error0))
#
#for i in range(16,31):
#    lanti_history_list, long_history_list, frequ_history_list = load_follow_date(
#                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i), 24)
#    lanti_list, long_list, frequ_list = load_accurate_date(
#                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
#    serverswitch = ServerSwitchDemandFollow(loca_array, 80, 50, lanti1, long1, server_placement_max_flow,
#                                    24, lanti_history_list, long_history_list, frequ_history_list,
#                                    5, lanti_list, long_list, frequ_list, 5)
#    result_list, final_optimal = serverswitch.server_switch(1)
#    result = array_to_list(result_list)
#    save_list_2D(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_follow_relative2/',
#                                    'server_scheduling_' + str(i) + '_follow_use100.txt', result)
#    predict_scheduling = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_follow_relative2/', 
#                                    'server_scheduling_' + str(i) + '_follow_use100.txt')
#    predict_scheduling_array = np.array(predict_scheduling)
#    sum_error0, error0 = serverswitch.service_failure_statistic(predict_scheduling_array, 1)
#    save_data(ROOT_PATH + '/all_data/server_scheduling_result/follow_relative_error2/', 'follow_' + str(i) + '_use100_error.txt', list(sum_error0))
#
#for i in range(25,26):
#    lanti_list, long_list, frequ_list = load_accurate_date(
#                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
#    serverswitch = ServerSwitchDemandPredict(loca_array, 80, 50, lanti1, long1, server_placement_max_flow,
#                                    24, 0, lanti_list, long_list, frequ_list, 5)
#    result_list, final_optimal = serverswitch.server_switch(1, 1, 1)
#    result = array_to_list(result_list)
#    save_list_2D(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_predict_relative/',
#                                    'server_scheduling_' + str(i) + '_error30_use100.txt', result)
#    predict_scheduling = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_predict_relative/', 
#                                    'server_scheduling_' + str(i) + '_error30_use100.txt')
#    predict_scheduling_array = np.array(predict_scheduling)
#    sum_error0, error0 = serverswitch.service_failure_statistic(predict_scheduling_array, 1)
#    save_data(ROOT_PATH + '/all_data/server_scheduling_result/predict_relative_error/', 'predict_' + str(i) + '_use100_error30.txt', list(sum_error0))

#for i in range(16,31):
#    lanti_list, long_list, frequ_list = load_accurate_date(
#                                    ROOT_PATH + '/all_data/data_division_one_day_frequ/', str(i))
#    lanti_history_list, long_history_list, frequ_history_list = load_accurate_date(
#                                    ROOT_PATH + '/all_data/data_division_five_day_frequ/', str(i - 5))
#
#    serverswitch = ServerSwitchEnergy(loca_array, 80, 50, lanti1, long1, server_placement_max_flow,
#                                    24, lanti_history_list, long_history_list, frequ_history_list,
#                                    1, lanti_list, long_list, frequ_list, 5)
#    result_list, final_optimal = serverswitch.server_switch(1, 1, 1)
#    result = array_to_list(result_list)
#    save_list_2D(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_energy_relative2/',
#                                    'server_scheduling_' + str(i) + '_energy_use100.txt', result)
#    predict_scheduling = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_energy_relative2/', 
#                                    'server_scheduling_' + str(i) + '_energy_use100.txt')
#    predict_scheduling_array = np.array(predict_scheduling)
#    sum_error0, error0 = serverswitch.service_failure_statistic(predict_scheduling_array, 1)
#    save_data(ROOT_PATH  + '/all_data/server_scheduling_result/energy_relative_error2/', 'energy_' + str(i) + '_use100_error.txt', list(sum_error0))

print('part5:Aimging to evaluate the usage of the energy')
energy_list = os.listdir(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_energy_relative2/')
energy_consume_list = []
for i in energy_list:
    energy_scheduling = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_energy_relative2/', i)
    energy_scheduling_array = np.array(energy_scheduling)
    energy_evaluation = ServerSchedulingEvalutionEnergy(energy_scheduling_array, 1, 1)
    energy_consumption = energy_evaluation.scheduling_evaluation()
    energy_consume_list.append(energy_consumption)
save_data(ROOT_PATH + '/all_data/server_scheduling_result/energy_relative_consumption2/', 'energy_use100_consume.txt', energy_consume_list)

#follow_list = os.listdir(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_follow_relative2/')
#energy_consume_list = []
#for i in follow_list:
#    energy_scheduling = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_follow_relative2/', i)
#    energy_scheduling_array = np.array(energy_scheduling)
#    energy_evaluation = ServerSchedulingEvalutionEnergy(energy_scheduling_array, 1, 1)
#    energy_consumption = energy_evaluation.scheduling_evaluation()
#    energy_consume_list.append(energy_consumption)
#save_data(ROOT_PATH + '/all_data/server_scheduling_result/follow_relative_consumption2/', 'follow_use100_consume.txt', energy_consume_list)

#predict_list = os.listdir(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_predict_relative/')
#energy_consume_list = []
#for i in predict_list:
#    energy_scheduling = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_predict_relative/', i)
#    energy_scheduling_array = np.array(energy_scheduling)
#    energy_evaluation = ServerSchedulingEvalutionEnergy(energy_scheduling_array, 1, 1)
#    energy_consumption = energy_evaluation.scheduling_evaluation()
#    energy_consume_list.append(energy_consumption)
#save_data(ROOT_PATH + '/all_data/server_scheduling_result/predict_relative_consumption/', 'predict_use100_error_30_consume.txt', energy_consume_list)

#day_night_list = os.listdir(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_day_night_relative/')
#energy_consume_list = []
#for i in day_night_list:
#    energy_scheduling = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/server_scheduling_day_night_relative/', i)
#    energy_scheduling_array = np.array(energy_scheduling)
#    energy_evaluation = ServerSchedulingEvalutionEnergy(energy_scheduling_array, 1, 1)
#    energy_consumption = energy_evaluation.scheduling_evaluation()
#    energy_consume_list.append(energy_consumption)
#save_data(ROOT_PATH + '/all_data/server_scheduling_result/day_night_consumption/', 'day_night_use60_consume.txt', energy_consume_list)


#print('part6: Aiming to analyze the result')
#energy_error_list = os.listdir(ROOT_PATH + '/all_data/server_scheduling_result/energy_relative_error2/')
#follow_error_list = os.listdir(ROOT_PATH + '/all_data/server_scheduling_result/follow_relative_error2/')
#day_night_error_list = os.listdir(ROOT_PATH + '/all_data/server_scheduling_result/day_night_relative_error/')
#predict_error_list = os.listdir(ROOT_PATH + '/all_data/server_scheduling_result/predict_error/')
#energy_list = []
#follow_list = []
#day_night_list = []
#predict_list = []
#for i in range(len(energy_error_list)):
#    energy = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/energy_relative_error2/', energy_error_list[i])
#    energy_list.append(energy[0])
#    follow = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/follow_relative_error2/', follow_error_list[i])
#    follow_list.append(follow[0])
#    day_night = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/day_night_relative_error/', day_night_error_list[i])
#    day_night_list.append(day_night[0])
#    predict = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/predict_relative_error/', predict_error_list[i])
#    predict_list.append(predict[0])
#energy_consume = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/energy_relative_consumption2/', 'energy_use100_consume.txt')
#follow_consume = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/follow_relative_consumption2/', 'follow_use100_consume.txt')
#predict_consume = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/predict_relative_consumption/', 'predict_use100_error_30_consume.txt')
#day_night_consume = load_data_float(ROOT_PATH + '/all_data/server_scheduling_result/day_night_relative_consumption/', 'day_night_use60_consume.txt')
#energy_array = np.array(energy_list)
#follow_array = np.array(follow_list)
#day_night_array = np.array(day_night_list)
#predict_array = np.array(predict_list)
#energy_ave = sum(energy_array.transpose()) / (24 * base_num)
#follow_ave = sum(follow_array.transpose()) / (24 * base_num)
#predict_ave = sum(predict_array.transpose()) / (24 * base_num)
#day_night_ave = sum(day_night_array.transpose()) / (24 * base_num)
#x_array = np.array(list(range(15)))
#plt.ylim([0,0.3])
#plt.xlabel('days_label')
#plt.ylabel('error rate/%')
#plt.plot(x_array, energy_ave, label = 'energy_scheduling')
#plt.plot(x_array, follow_ave, label = 'follow_scheduling')
#plt.plot(x_array, predict_ave,label = 'predict_scheduling')
#plt.plot(x_array, day_night_ave, label = 'day_night_scheduling')
#plt.legend(loc=0,ncol=2)
#plt.show()
#
#plt.ylim([0,300000])
#plt.xlabel('days_label')
#plt.ylabel('energy_consumption')
#plt.plot(x_array, np.array(energy_consume[0]), label = 'energy_scheduling')
#plt.plot(x_array, np.array(follow_consume[0]), label = 'follow_scheduling')
#plt.plot(x_array, np.array(predict_consume[0]),label = 'predict_scheduling')
#plt.plot(x_array, np.array(day_night_consume[0]), label = 'day_night_scheduling')
#plt.legend(loc=0,ncol=2)
#plt.show()









