# -*- coding: utf-8 -*-


import math
import numpy as np

    
class ServerPlacementBasic():
    
    """
    Basic class for server placement
    """
    
    def _init_(self,compute_num):
        self.compute_num = compute_num
    
    def calc_distance(self, lat_a, lng_a, lat_b, lng_b):
        """calculate the actual distance between location_a and location_b
        
        param
        ----------
            lat_a : float
                lantitude of the location_a
            lng_a : float
                longitude of the location_a
            lat_b : float
                lantitude of the location_b
            lng_b : float
                longitude of the location_b
                
        return
        ----------
            12742 * math.asin(math.sqrt(a)) : float
                the distance information between the two location
        
        refer
        ----------
            Other func : None
        """
        p = 0.017453292519943295 
        
        a = (0.5 - math.cos((lat_b - lat_a) * p) / 2 + math.cos(lat_a * p) * math.cos(lat_b * p) 
        * (1 - math.cos((lng_b - lng_a) * p)) / 2)
        return 12742 * math.asin(math.sqrt(a)) 

    def array_calculate_distance(self,location_a, location_b):
        """calculate the actual distance between the element in loca_a and loca_b
        
        param
        ----------
            location_a : list
                store all the location information in first location_list
            location_b :list
                store all the location information in second location_list
               
        return
        ----------
            12742 * np.arcsin(np.sqrt(a)) : list
                store all the distance information between the elements in the two lists
        
        refer
        ----------
            Other func : None
        """
        p = 0.017453292519943295
        
        a = (0.5 - np.cos((location_b[:,0] - location_a[:,0]) * p) / 2 + np.cos(location_a[:,0] * p) 
        * np.cos(location_b[:,0] * p) * (1 - np.cos((location_b[:,1] - location_a[:,1]) * p)) / 2)
        return 12742 * np.arcsin(np.sqrt(a))
    
    def search_base_indistance(self, location_a, location_b_array, distance):
        location_a_array = np.zeros((len(location_b_array), 2))
        for i in range(len(location_b_array)):
            location_a_array[i,:] = location_a
        distance_array = self.array_calculate_distance(location_a_array, location_b_array)
        location_num = 0
        for i in range(len(distance_array)):
            if distance_array[i] <= distance:
                location_num += 1
        return location_num
    
    def server_placement(self):
        pass

    
