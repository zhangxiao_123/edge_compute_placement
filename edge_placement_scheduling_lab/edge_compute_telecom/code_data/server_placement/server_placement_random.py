# -*- coding: utf-8 -*-


import random
from server_placement.server_placement_basic import ServerPlacementBasic

class ServerPlacementRandom(ServerPlacementBasic):
    
    """
    We assign the servers to the cluster just using the random
    """
    
    def __init__(self, base_num, compute_num, top, low):
        """
        
        param
        ----------
            base_num : int
                the number of the base station we want to assign
            compute_num : int
                the number of the servers we want to assign
            top : int
                the highest numbers of the servers we will assign
            low : int
                the lowest numbers of the servers we will assign
        """
        ServerPlacementBasic._init_(self, compute_num)
        self.base_num = base_num
        self.top = top
        self.low = low
        
    def server_placement(self):
        """assign the server with rand
        
        param
        ----------
            we can get them in the defination of the init
        
        return
        ----------
            final_list : list
                store the distribution of the servers
        """
        list_rand = []
        temp = 0
        
        while(not (self.compute_num - temp >= self.low and self.compute_num - temp <= self.top)):
            temp_1 = random.randint(self.low, self.top)
            temp = temp + temp_1
            list_rand.append(temp_1)
        list_rand.append(self.compute_num - temp)
        rand_size = len(list_rand)
        index_list = []
        
        while(len(index_list) < rand_size):
            x=random.randint(0, self.base_num-1)
            if x not in index_list:
                index_list.append(x)
        final_list = [0 for i in range(self.base_num)]
        
        for i in range(len(index_list)):
            final_list[index_list[i]] = list_rand[i]
        return final_list
    
