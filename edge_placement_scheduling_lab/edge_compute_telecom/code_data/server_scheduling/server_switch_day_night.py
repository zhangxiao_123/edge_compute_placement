# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 21:17:39 2021

@author: 98330
"""

import numpy as np
import root_const
import math
from base_utils.my_utils import load_json, save_json
from root_const import ROOT_PATH
import gurobipy as gr
from server_scheduling.server_switch_basic import ServerSwitchBasic

class ServerSwitchDayNight(ServerSwitchBasic):
    
    def __init__(self, location_array, k_near, k_relative,lanti1, long1, server_placement, time_interval,
                 lanti_list, long_list, frequ_list,adjust_rate):
        """
        
        param
        ----------
            location_array : array
                the array to store both the lantitude and longtitude information of the location
                structure : nx2
                content : first column ---> lantitude , second column ---> longitude
            k_near : int
                the number of the near base station we will find
            compute_num : int
                the number of the servers we want to assign
            workload : list
                store the information about the load
        """
        ServerSwitchBasic.__init__(self, location_array, k_near)                  
        self.lanti1 = lanti1
        self.long1 = long1
        self.server_placement = server_placement
        self.time_interval = time_interval
        self.lanti_list = lanti_list
        self.long_list = long_list
        self.frequ_list = frequ_list
        self.adjust_rate = adjust_rate
        self.k_relative = k_relative
    
    def find_relative_loca(self):
        union_relative = load_json(ROOT_PATH + '/all_data/server_scheduling_result/server_union_statistic/', 
                          'server_union.json')
        list_relative = []
        list_relative_assign = []
        for i in range(len(union_relative)):
            list_relative_assign.append([])    
        for i in range(len(union_relative)):
            dict_temp = union_relative[i]
            dict_sort = sorted(dict_temp.items(),key=lambda dict_temp:dict_temp[1],reverse=True)
            dict_key = [dict_sort[i][0] for i in range(len(dict_sort))]
            dict_key = list(map(int, dict_key))
            list_relative.append(dict_key[0:self.k_relative])
            for j in range(self.k_relative):
                list_relative_assign[list_relative[i][j]].append(i)
        return list_relative, list_relative_assign

    def load_distribution(self, lanti_1, long_1, lanti_2, long_2, load):
        """find the locations in the loca2 that are emerged in the loca1 
        
        param
        -----------
            lanti_1 : list
                store all the lantitude information of the loca1
            lanti_2 : list
                store all the lantitude information of the loca2
            long_1 : list
                store all the longtitude information of the loca1
            long_2 : list
                store all the longtitude information of the loca2
            load : list
                store all the load information of the loca2
            
        return
        ----------
            load_rebuild : list
                store the load in both loca1 and loca2, others are 0
            num : int
                the number of the loca included in both loca1 and loca2
        
        refer
        ----------
            Other func : None
        """
        base_num = len(lanti_1)
        num = 0
        load_rebuild = [0 for i in range(base_num)]
        
        for i in range(len(lanti_2)):    
            try:
                lanti_index = lanti_1.index(lanti_2[i])
            except:
                lanti_index = -1
        
            try:
                long_index = long_1.index(long_2[i])
            except:
                long_index = -2
                
            if lanti_index == long_index:
                load_rebuild[lanti_index] = load[i]
                num = num + 1
        return load_rebuild, num
    
    def sum_distribution(self):
        """
        rebuild the data to change one day data into several part
        """
        final_rebuild = []
        
        for i in range(len(self.lanti_list)):
            load_rebuild, num = self.load_distribution(self.lanti1, self.long1, 
                                                       self.lanti_list[i], self.long_list[i], self.frequ_list[i])
            final_rebuild.append(list(np.array(load_rebuild) * self.adjust_rate))
        return final_rebuild
        
    def server_switch(self, start_time, end_time, operate_rate):
        time_part = int(24 / self.time_interval)
        start_time_hour = int(start_time[0 : start_time.find(':')])
        start_time_minute = int(start_time[start_time.find(':')+1 : start_time.find(':')+3])
        start_time_num = start_time_hour + start_time_minute / 60
        end_time_hour = int(end_time[0 : end_time.find(':')])
        end_time_minute = int(end_time[end_time.find(':')+1 : end_time.find(':')+3])
        end_time_num = end_time_hour + end_time_minute / 60
        
        day_start_index = int(start_time_num / time_part)
        day_end_index = int(end_time_num / time_part)
        server_arrange = np.zeros((self.time_interval, len(self.server_placement)))
        for i in range(self.time_interval):
            server_arrange[i,:] = np.array(self.server_placement)
        
        server_arrange[:day_start_index,:] = operate_rate *  server_arrange[:day_start_index,:]
        server_arrange[day_end_index:,:] = operate_rate *  server_arrange[day_end_index:,:]
        
        return server_arrange
    
    
    def server_usage(self, y_array):
      
#        list_near, list_far, list_assign = self.location_divide()
        list_near, list_assign = self.find_relative_loca()
        final_rebuild = self.sum_distribution()
        time_part = len(final_rebuild)
        base_num = len(list_near)
        list_var = [] 
        
        model_placement = gr.Model('Lip_placement')
        y = model_placement.addVars(time_part, base_num, vtype='S', lb=0)
        a = model_placement.addVars(time_part, vtype='S', lb=0)  
        u = model_placement.addVars(time_part, base_num, len(list_near[0]), vtype='S', lb=0)                
        
        model_placement.setObjective(sum(a[i] for i in range(time_part)) , gr.GRB.MAXIMIZE)
        for i in range(time_part):
            model_placement.addConstrs(
                    y[i,j] <= y_array[i,j] for j in range(base_num)
            ) 
            model_placement.addConstrs(
                    sum(u[i,k,j] for j in range(len(list_near[0]))) == a[i]
                    for k in range(base_num)
            )
        for k in range(time_part):
            model_placement.addConstrs(
                    sum(u[k,list_assign[i][j],list_near[list_assign[i][j]].index(i)] * final_rebuild[k][list_assign[i][j]] 
                    for j in range(len(list_assign[i]))) <= y[k,i] 
                    for i in range(base_num)
             ) 
        model_placement.optimize() 
       
        if model_placement.Status == gr.GRB.OPTIMAL:
            for var in model_placement.getVars():
                list_var.append(var.x)
            result_list = np.array(list_var[0: base_num * time_part])
            result_list = result_list.reshape((time_part,base_num))
            final_optimal = list_var[base_num * time_part : base_num * time_part + time_part]
            u_list = np.array(list_var[base_num * time_part + time_part : ])
            u_array = u_list.reshape((time_part, base_num, len(list_near[0])))
        return result_list, final_optimal, u_array, list_assign, final_rebuild, base_num, list_near

    def service_failure_statistic(self, y_array, max_rate):
        result_list, final_optimal, u_array, list_assign, final_rebuild, base_num, list_near = self.server_usage(y_array)
        service_failed_num = []
        a = 1 / max_rate
        for k in range(len(final_optimal)):
            failed_num = 0
            if final_optimal[k] >= a:
                service_failed_num.append(failed_num)
                continue
            for i in range(base_num):
                service_temp = 0
                for j in range(len(list_assign[i])):
                    service_temp = service_temp + u_array[k,list_assign[i][j],list_near[list_assign[i][j]].index(i)] * final_rebuild[k][list_assign[i][j]] 
                if service_temp > result_list[k,i] * final_optimal[k]:
                    failed_num = failed_num + 1
            service_failed_num.append(failed_num)
        return np.array(service_failed_num), np.array(service_failed_num) / base_num   
        
            
        
        
        
      


