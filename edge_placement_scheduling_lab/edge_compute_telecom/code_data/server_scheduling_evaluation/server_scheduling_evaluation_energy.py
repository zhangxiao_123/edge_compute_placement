# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 15:27:49 2021

@author: 98330
"""
import numpy as np
from server_scheduling_evaluation.server_scheduling_evaluation_basic import ServerSchedulingEvaluationBasic

class ServerSchedulingEvalutionEnergy(ServerSchedulingEvaluationBasic):
    
    def __init__(self, scheduling_array, operation_consumption, switch_consumption):
        ServerSchedulingEvaluationBasic.__init__(self,scheduling_array)
        self.operation_consumption = operation_consumption
        self.switch_consumption = switch_consumption
        
    def scheduling_evaluation(self):
        total_operation_time = np.sum(self.scheduling_array)
        total_operation_consumption = total_operation_time * self.operation_consumption
        
        time_num = len(self.scheduling_array)
        switch_times = np.zeros((time_num - 1,len(self.scheduling_array[0,:])))
        
        for i in range(time_num - 1):
            switch_time = np.abs(self.scheduling_array[i + 1,:] - self.scheduling_array[i,:])
            switch_times[i,:] = switch_time
            
        total_switch_times = np.sum(switch_times)
        total_switch_consumption = total_switch_times * self.switch_consumption
        
        total_energy_consumption = total_operation_consumption + total_switch_consumption
        return total_energy_consumption
        
            
        
        